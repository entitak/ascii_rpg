import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Editor from './components/Editor/Editor';
import { Game } from './components/Game/Game';
import { GlobalStyle } from './components/GlobalStyle';

ReactDOM.render(
  <Router>
    <GlobalStyle />
    <Route exact path="/">
      <Game />
    </Route>
    <Route exact path="/editor">
      <Editor />
    </Route>
  </Router>,
  document.getElementById('root')
);
