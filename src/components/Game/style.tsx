import styled from "styled-components";

export const WindowBackground = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: black;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const MainCanvas = styled.canvas<any>`
  border: none;
`;

export const HitboxCanvas = styled(MainCanvas)`
  position: absolute;
  z-index: 9;
  opacity: 0;
`;
