import { useEffect, useState, useRef } from "react";
import Engine from "../Classes/Engine";
import setting from "../Settings/settings.json";
import { HitboxCanvas, MainCanvas, WindowBackground } from "./style";

export const Game = () => {
  const gameSize = { width: setting.gameSize.width, height: setting.gameSize.height };
  const [pixelSize, setPixelSize] = useState({
    width: ~~(window.innerWidth / gameSize.width),
    height: ~~(window.innerHeight / gameSize.height)
  })
  const mainCanvasRef = useRef<HTMLCanvasElement>(null);
  const hitboxCanvasRef = useRef<HTMLCanvasElement>(null);
  const [engine, setEngine] = useState<any>();

  const getEngineSetting = () => {
    let mainCanvas = mainCanvasRef.current;
    let hitboxCanvas = hitboxCanvasRef.current;
    if (!mainCanvas || !hitboxCanvas) return;
    const mainCtx = mainCanvas.getContext('2d');
    const hitboxCtx = hitboxCanvas.getContext('2d');

    mainCanvas.width = hitboxCanvas.width = pixelSize.width * gameSize.width;
    mainCanvas.height = hitboxCanvas.height = pixelSize.height * gameSize.height;

    return {
      width: gameSize.width,
      height: gameSize.height,
      pixelWidth: pixelSize.width,
      pixelHeight: pixelSize.height,
      mainCtx: mainCtx,
      hitboxCtx: hitboxCtx
    }
  }

  useEffect(() => {
    if (!engine) {
      setEngine(new Engine(getEngineSetting()))
      return;
    }

    engine.start();
    window.addEventListener("resize", updateToRedraw);
    engine.inputManager.bindKeys();
    engine.inputManager.bindClick();
    engine.inputManager.bindMouseMove();

    return () => {
      engine.end();
      window.removeEventListener("resize", updateToRedraw);
      engine.inputManager.unbindKeys();
      engine.inputManager.unbindClick();
      engine.inputManager.unbindMouseMove();
    }
    // eslint-disable-next-line
  }, [engine]);

  useEffect(() => {
    if (!engine) return;
    const engineSetting = getEngineSetting();
    let newEngine = new Engine(engineSetting);
    engine.resizeUpdate(engineSetting);
    Object.assign(newEngine, engine);
    setEngine(newEngine);
    // eslint-disable-next-line
  }, [pixelSize]);

  function updateToRedraw() {
    setPixelSize({
      width: ~~(window.innerWidth / gameSize.width),
      height: ~~(window.innerHeight / gameSize.height)
    });
  }

  return (
    <WindowBackground>
      <MainCanvas ref={mainCanvasRef} />
      <HitboxCanvas ref={hitboxCanvasRef} />
    </WindowBackground>
  );
};
