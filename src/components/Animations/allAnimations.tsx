import campfire from './Campfire.json';
import inventoryPlayerFirstFigure from './InventoryPlayerFirstFigure.json'

export const animationsToInputFormat = (animation: any) => {
    const newArray = JSON.parse(JSON.stringify(animation));
    newArray.forEach((anim: any) => {
        anim.data.forEach((tile: any) => {
            tile.top += anim.y;
            tile.left += anim.x;
        });
    })

    const resultArray: any = [];
    newArray.forEach((array: any) => {
        resultArray.push(array.data);
    })

    return resultArray;
}

export const allAnimations = [
    {
        name: "Campfire",
        animation: animationsToInputFormat(campfire)
    },
    {
        name: "Inventory-Player => First figure",
        animation: animationsToInputFormat(inventoryPlayerFirstFigure)
    },
]