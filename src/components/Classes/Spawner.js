import { getRandomInt } from "../utils";
import Loot from "./Entities/Loot";
import Monster from "./Entities/Monster";
import lootTable from "../SpawnTables/LootTable.json";
import monsterTable from "../SpawnTables/MonsterTable.json";

class Spawner {
  constructor(maps) {
    this.maps = maps;
  }

  spawn(spawnCount, createEntity) {
    for (let count = 0; count < spawnCount; count++) {
      this.maps.lastId++;
      let entity = createEntity();
      entity.id = this.maps.lastId;
      this.maps.createOnEmptySpace(entity);
    }
  }

  spawnLoot(spawnCount) {
    this.spawn(spawnCount, () => {
      let lootSetting = {
        ...lootTable[getRandomInt(lootTable.length)],
        x: getRandomInt(this.maps.getSelectedMap().width),
        y: getRandomInt(this.maps.getSelectedMap().height),
        context: this.maps.mainCtx,
      };
      return new Loot(lootSetting);
    });
  }

  spawnMonters(spawnCount) {
    this.spawn(spawnCount, () => {
      let monsterSetting = {
        ...monsterTable[getRandomInt(monsterTable.length)],
        x: getRandomInt(this.maps.getSelectedMap().width),
        y: getRandomInt(this.maps.getSelectedMap().height),
        context: this.maps.mainCtx,
      };

      return new Monster(monsterSetting);
    });
  }
}

export default Spawner;
