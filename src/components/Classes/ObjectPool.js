import Player from "./Entities/Player";

class ObjectPool {
  constructor(maps, name) {
    this.name = name;
    this.maps = maps;
    this.objects = [];
  }

  add(object) {
    this.objects.push(object);
  }

  remove(object) {
    this.objects = this.objects.filter((obj) => obj !== object);
  }

  checkCollision(checkObject, origObject) {
    let collision = false;
    this.objects.forEach((object) => {
      if (
        checkObject.x < object.x + object.width &&
        checkObject.x + checkObject.width > object.x &&
        checkObject.y < object.y + object.height &&
        checkObject.y + checkObject.height > object.y &&
        object.id !== origObject?.id
      ) {
        if (origObject instanceof Player) object.action("bump", this.maps);
        collision = true;
        return;
      }
    });
    return collision;
  }
}

export default ObjectPool;
