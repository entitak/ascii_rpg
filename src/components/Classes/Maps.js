import Player from "./Entities/Player";
import Wall from "./Entities/Wall";
import ObjectPool from "./ObjectPool";
import Spawner from "./Spawner";

export default class Maps {
  constructor(setting) {
    this.setting = setting;
    this.width = setting.width;
    this.height = setting.height;
    this.mainCtx = setting.mainCtx;
    this.hitboxCtx = setting.hitboxCtx;
    this.lastId = -1;
    this.map = [
      {
        name: "camp",
        width: 96,
        height: 32,
        pool: this.createCampsite(96, 32),
      },
    ];
    this.selectedMap = "camp";
    this.spawner = new Spawner(this);
    // this.camera = new Camera(gameCanvas);
    this.createPlayerOnEmptySpace();
    this.spawn();
  }

  spawn() {
    const spawner = this.spawner;
    spawner.spawnMonters(10);
    spawner.spawnLoot(10);
  }

  getPlayer() {
    return this.getSelectedPool().objects.filter((object) => {
      return object instanceof Player;
    })[0];
  }

  createOnEmptySpace(object) {
    const thisMap = this.getSelectedMap();
    for (let col = 0; col < thisMap.height - object.height; col++) {
      for (let row = 0; row < thisMap.width - object.width; row++) {
        if (!thisMap.pool.checkCollision(object)) {
          thisMap.pool.add(object);
          return;
        }
        object.moveTo(row, col);
      }
    }
  }

  createPlayerOnEmptySpace() {
    const playerSetting = {
      id: this.lastId + 1,
      x: 0,
      y: 0,
      context: this.mainCtx,
    };
    this.lastId++;
    const player = new Player(playerSetting);
    this.createOnEmptySpace(player);
  }

  draw() {
    this.getSelectedPool().objects.forEach((object) => {
      object.draw(this.setting.pixelWidth, this.setting.pixelHeight);
    });
  }

  createCampsite(width, height) {
    const pool = new ObjectPool(this, "Walls");
    let wallSetting = {
      id: this.lastId + 1,
      x: 0,
      y: 0,
      width: 1,
      height: 1,
      data: [["#"]],
      color: [["brown"]],
      context: this.mainCtx,
    };
    // Horizontal border walls
    for (let col = 0; col < width; col++) {
      for (let i = 0; i < 2; i++) {
        wallSetting.id = this.lastId;
        wallSetting.x = col;
        wallSetting.y = i * (height - 1);
        pool.add(new Wall(wallSetting));
        this.lastId++;
      }
    }
    // Vertical border walls
    for (let row = 1; row < height - 1; row++) {
      for (let i = 0; i < 2; i++) {
        wallSetting.id = this.lastId;
        wallSetting.x = i * (width - 1);
        wallSetting.y = row;
        pool.add(new Wall(wallSetting));
        this.lastId++;
      }
    }
    return pool;
  }

  getSelectedMap() {
    for (let i = 0; i < this.map.length; i++) {
      if (this.map[i].name === this.selectedMap) return this.map[i];
    }
  }

  getSelectedPool() {
    return this.getSelectedMap().pool;
  }
}
