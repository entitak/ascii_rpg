import setting from "../Settings/settings.json";
import { findObjByPropInNested, rgbToHex } from "../utils";

class InputManager {
  constructor(inventory, menu, maps) {
    this.inventory = inventory;
    this.menu = menu;
    this.maps = maps;
    this.ableToMove = true;
    this.keys = [];
    this.pressed = [];

    this.fps = setting.fps;
    this.fpsInterval = 1000 / this.fps;
    this.then = Date.now();
  }

  toggleInventory() {
    if (!this.menu.open) this.ableToMove = !this.ableToMove;
    if (!this.menu.open) this.inventory.open = !this.inventory.open;
  }

  toggleMenu() {
    if (!this.inventory.open) this.ableToMove = !this.ableToMove;
    this.menu.open = !this.menu.open;
  }

  tick() {
    const now = Date.now();
    const elapsed = now - this.then;

    if (elapsed > this.fpsInterval) {
      this.then = now - (elapsed % this.fpsInterval);

      if (Object.keys(this.keys).length !== 0 && this.ableToMove)
        this.handleMovement();
      if (Object.keys(this.pressed).length !== 0) this.handleKeys();
    }
  }

  handleMovement() {
    const player = this.maps.getPlayer();
    const playerCopy = player.copyPlayer();
    const pool = this.maps.getSelectedMap().pool;
    if (this.keys[37]) {
      //left arrow
      playerCopy.move(-player.velocityX, 0);
      if (pool.checkCollision(playerCopy, player)) return;
      player.move(-player.velocityX, 0);
    }
    if (this.keys[38]) {
      //up arrow
      playerCopy.move(0, -player.velocityY);
      if (pool.checkCollision(playerCopy, player)) return;
      player.move(0, -player.velocityY);
    }
    if (this.keys[39]) {
      //right arrow
      playerCopy.move(player.velocityX, 0);
      if (pool.checkCollision(playerCopy, player)) return;
      player.move(player.velocityX, 0);
    }
    if (this.keys[40]) {
      //down arrow
      playerCopy.move(0, player.velocityY);
      if (pool.checkCollision(playerCopy, player)) return;
      player.move(0, player.velocityY);
    }
  }

  handleKeys() {
    if (this.pressed[27]) {
      //Esc key
      delete this.pressed[27];
      this.toggleMenu();
    }
    if (this.pressed[69]) {
      //E key
      delete this.pressed[69];
      this.toggleInventory();
    }
    if (this.pressed[9]) {
      //Tab key
      delete this.pressed[9];
      this.toggleInventory();
    }
  }

  pressedKey = (e) => {
    if (e.key !== "F5" && e.key !== "F12") e.preventDefault();
    this.keys[e.keyCode] = true;
    if (
      e.keyCode !== 37 &&
      e.keyCode !== 38 &&
      e.keyCode !== 39 &&
      e.keyCode !== 40
    )
      this.pressed[e.keyCode] = true;
  };

  unpressedKey = (e) => {
    delete this.keys[e.keyCode];
  };

  handleClick = (e) => {
    let pixelData = this.maps.hitboxCtx.getImageData(
      e.clientX - e.target.getBoundingClientRect().x,
      e.clientY - e.target.getBoundingClientRect().y,
      1,
      1
    ).data;
    let color = rgbToHex(pixelData[0], pixelData[1], pixelData[2]);
    let clicked = findObjByPropInNested(this.inventory.hitbox, "color", color);
    if (!clicked) return;
    this.inventory.getItemFromObj(clicked);
  };

  handleMouseMove = (e) => {
    document.body.style.cursor = "default";
    let pixelData = this.maps.hitboxCtx.getImageData(
      e.clientX - e.target.getBoundingClientRect().x,
      e.clientY - e.target.getBoundingClientRect().y,
      1,
      1
    ).data;
    let color = rgbToHex(pixelData[0], pixelData[1], pixelData[2]);
    if (color === "#000000") return;
    let found = findObjByPropInNested(this.inventory.hitbox, "color", color);
    if (found)
      if (this.inventory.isClickable(found))
        document.body.style.cursor = "pointer";
  };

  bindMouseMove() {
    document.addEventListener("mousemove", this.handleMouseMove);
  }

  unbindMouseMove() {
    document.removeEventListener("mousemove", this.handleMouseMove);
  }

  bindClick() {
    document.addEventListener("click", this.handleClick);
  }

  unbindClick() {
    document.removeEventListener("click", this.handleClick);
  }

  bindKeys() {
    document.addEventListener("keydown", this.pressedKey);
    document.addEventListener("keyup", this.unpressedKey);
  }

  unbindKeys() {
    document.removeEventListener("keydown", this.pressedKey);
    document.removeEventListener("keyup", this.unpressedKey);
  }
}

export default InputManager;
