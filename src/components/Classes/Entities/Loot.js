import Entity from "./Entity";

class Loot extends Entity {
  constructor(setting) {
    super(setting);
    this.name = setting.name;
    this.type = setting.type;
    this.availableSlots = setting.availableSlots;
    this.offset = setting.offset;
    this.health = setting.health;
    this.damage = setting.damage;
    this.armor = setting.armor;
  }

  action(verb, maps) {
    if (verb === "bump") {
      if (maps.getPlayer().isInventoryFull()) {
        console.log("Inventory is full!");
        return;
      }
      console.log(`Picked up ${this.name}!`);
      maps.getPlayer().add(this);
      maps.getSelectedPool().remove(this);
    } else if (verb === "drop") {
      console.log("drop", this);
    }
  }

  use(action, maps) {
    switch (action) {
      case "USE":
        if (this.type === "potion") {
          maps.getPlayer().heal(this);
          return null;
        }
        console.log("This item doesn't have an action!");
        return this;
      case "EQUIP":
        maps.getPlayer().equipItem(this.availableSlots[0], this);
        return this;
      case "UNEQUIP":
        if (maps.getPlayer().isInventoryFull()) {
          console.log("Inventory is full!");
        } else {
          maps.getPlayer().unequipItem(this);
        }
        return this;
      default:
        break;
    }
  }
}

export default Loot;
