import settings from "../../Settings/settings.json";
import { getFontSize } from "../../utils";

class Entity {
  constructor(setting) {
    this.id = setting.id;
    this.x = setting.x || 0;
    this.y = setting.y || 0;
    this.width = setting.width;
    this.height = setting.height;
    this.data = setting.data;
    this.color = setting.color || "#F5F5F5";
    this.context = setting.context;
  }

  action(verb, world) {
    console.log(
      `${verb} to ${this.name} from ${this.constructor.name}'s group`
    );
  }

  draw(pixelWidth, pixelHeight) {
    // Redraw background (replacing potencional previous char)
    this.context.fillStyle = "black";
    this.context.fillRect(
      this.x * pixelWidth,
      this.y * pixelHeight,
      pixelWidth,
      pixelHeight
    );

    // Draw char
    this.context.font = getFontSize() + "px " + settings.fontFamily;
    this.context.textBaseline = "hanging";
    for (let col = 0; col < this.width; col++) {
      for (let row = 0; row < this.height; row++) {
        this.context.fillStyle = this.color[row][col];
        this.context.fillText(
          this.data[row][col],
          (this.x + col) * pixelWidth,
          (this.y + row) * pixelHeight,
          pixelWidth
        );
      }
    }
  }

  moveTo(dx, dy) {
    this.x = dx;
    this.y = dy;
  }

  move(dx, dy) {
    this.x += dx;
    this.y += dy;
  }
}

export default Entity;
