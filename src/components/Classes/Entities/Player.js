import Entity from "./Entity";
import inventorySetting from "../../Settings/inventory.json";
import { getNumberFromString } from "../../utils";

class Player extends Entity {
  constructor(setting) {
    super(setting);
    this.inventory = [];
    this.equip = this.createDefaultEquip();
    this.width = 3;
    this.height = 3;
    this.data = [
      ["", "O", ""],
      ["/", "|", "\\"],
      ["/", "", "\\"],
    ];
    this.color = [
      ["#F5F5F5", "#F5F5F5", "#F5F5F5"],
      ["#F5F5F5", "#F5F5F5", "#F5F5F5"],
      ["#F5F5F5", "#F5F5F5", "#F5F5F5"],
    ];
    this.velocityX = 1;
    this.velocityY = 0.5;
    this.stats = {
      health: 10,
      damage: 1,
      armor: 0,
    };
  }

  createDefaultEquip() {
    let equip = [];
    let equipNames = inventorySetting.equipAndStats.equip.names;
    let inventory = inventorySetting.inventoryAndHotbar;
    let hotbarSlots = inventory.hotbar.slotRowNum;
    let inventorySlots =
      inventory.inventory.slotColNum * inventory.inventory.slotRowNum;
    for (let i = 0; i < equipNames.length; i++) {
      equip.push({
        slot: "slot" + (i + hotbarSlots + inventorySlots),
        name: equipNames[i],
        item: null,
      });
    }
    return equip;
  }

  equipItem(slot, item) {
    let index = this.equip.map((obj) => obj.name).indexOf(slot);
    if (index !== -1) {
      let inventory = inventorySetting.inventoryAndHotbar;
      let hotbarSlots = inventory.hotbar.slotRowNum;
      let inventorySlots =
        inventory.inventory.slotColNum * inventory.inventory.slotRowNum;
      let lastItem = this.equip[index].item;
      let invIndex = this.inventory.map((obj) => obj.slot).indexOf(item.slot);
      this.inventory.splice(invIndex, 1);
      item.slot = "slot" + (index + hotbarSlots + inventorySlots);
      this.equip[index].item = item;
      if (lastItem) this.add(lastItem);
    } else console.log("This item's slot doesn't exists.");
  }

  unequipItem(item) {
    let index = this.equip.map((obj) => obj.slot).indexOf(item.slot);
    this.equip[index].item = null;
    item.slot = "slot" + this.getFirstEmptySlot();
    this.inventory.push(item);
  }

  getEquipedItem(type) {
    let item;
    // eslint-disable-next-line array-callback-return
    this.equip.map((obj) => {
      if (obj.name === type) item = obj.item;
    });
    return item;
  }

  getDamage() {
    let damage = this.stats.damage;

    this.equip.forEach((item) => {
      if (item?.item?.type === "attack" && item?.item?.damage)
        damage += item.item.damage;
    });
    return damage;
  }

  getArmor() {
    let armor = this.stats.armor;

    this.equip.forEach((item) => {
      if (item?.item?.type === "defence" && item?.item?.armor)
        armor += item.item.armor;
    });
    return armor;
  }

  getHealth() {
    let health = this.stats.health;

    this.equip.forEach((item) => {
      if (item?.item?.type === "defence" && item?.item?.health)
        health += item.item.health;
    });
    return health;
  }

  editHealth(health) {
    this.stats.health += health;
  }

  editArmor(armor) {
    this.stats.armor += armor;
  }

  editDamage(damage) {
    this.stats.damage += damage;
  }

  moveTo(dx, dy) {
    super.moveTo(dx, dy);
  }

  move(dx, dy) {
    super.move(dx, dy);
  }

  getFirstEmptySlot() {
    let settings = inventorySetting.inventoryAndHotbar;
    let hotbarSlots = settings.hotbar.slotRowNum;
    let inventorySlots =
      settings.inventory.slotColNum * settings.inventory.slotRowNum;
    let allNums = Array.from(Array(hotbarSlots + inventorySlots).keys());
    for (let i = 0; i < this.inventory.length; i++) {
      if (this.inventory[i]) {
        let slotIndex = getNumberFromString(this.inventory[i].slot);
        let idx = allNums.indexOf(slotIndex);
        allNums.splice(idx, 1);
      }
    }
    return allNums[0];
  }

  isInventoryFull() {
    let settings = inventorySetting.inventoryAndHotbar;
    let hotbarSlots = settings.hotbar.slotRowNum;
    let inventorySlots =
      settings.inventory.slotColNum * settings.inventory.slotRowNum;
    let maxItems = hotbarSlots + inventorySlots;
    if (this.inventory.length >= maxItems) return true;
    return false;
  }

  add(item) {
    item.slot = "slot" + this.getFirstEmptySlot();
    this.inventory.push(item);
  }

  remove(item) {
    this.inventory = this.inventory.filter((e) => e !== item);
  }

  heal(potion) {
    this.editHealth(potion.health);
    this.remove(potion);
  }

  copyPlayer() {
    let newPlayer = new Player({
      x: this.x,
      y: this.y,
      context: this.context,
    });
    Object.assign(newPlayer, this);
    return newPlayer;
  }
}

export default Player;
