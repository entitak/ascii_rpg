import Entity from "./Entity.js";

class Monster extends Entity {
  constructor(setting) {
    super(setting);
    this.name = setting.name;
    this.health = setting.health;
    this.damage = setting.damage;
    this.armor = setting.armor;
  }

  action(verb, maps) {
    if (verb === "bump") {
      console.log(`Player attacks ${this.name}!`);
      this.health = this.health - maps.getPlayer().getDamage();
      if (this.health <= 0) {
        console.log(`${this.name} dies!`);
        maps.getSelectedPool().remove(this);
        return;
      }
      console.log(`${this.name}'s health = ${this.health}`);
      let armor = maps.getPlayer().getArmor();
      const monsterDamage = this.damage - armor > 0 ? this.damage - armor : 0;
      maps.getPlayer().editHealth(-monsterDamage);
      if (maps.getPlayer().getHealth() <= 0) {
        console.log(`You have died!`);
        return;
      }
      console.log(`You have ${maps.getPlayer().getHealth()} health`);
    }
  }
}

export default Monster;
