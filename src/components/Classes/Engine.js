import InputManager from "./InputManager";
import Maps from "./Maps";
import Menu from "./Menu";
import Inventory from "./Inventory";

export default class Engine {
  constructor(setting) {
    const maps = new Maps(setting);
    const inventory = new Inventory(setting, maps);
    const menu = new Menu(setting);
    this.inputManager = new InputManager(inventory, menu, maps);
    this.maps = maps;
    this.menu = menu;
    this.inventory = inventory;
    this.mainCtx = setting.mainCtx;
    this.hitboxCtx = setting.hitboxCtx;
  }
  reqAnimationFrameRef = null;

  drawScene() {
    this.mainCtx.clearRect(
      0,
      0,
      this.mainCtx.canvas.width,
      this.mainCtx.canvas.height
    );
    this.hitboxCtx.clearRect(
      0,
      0,
      this.hitboxCtx.canvas.width,
      this.hitboxCtx.canvas.height
    );
    if (this.menu.open) this.menu.draw();
    else if (this.inventory.open) this.inventory.draw();
    else this.maps.draw();
  }

  drawHitbox() {
    if (this.menu.open) return;
    else if (this.inventory.open) this.inventory.drawHitboxes();
  }

  resizeUpdate(setting) {
    this.maps.setting = setting;
    this.inventory.setting = setting;
    this.menu.setting = setting;
  }

  tick() {
    if (this.maps.getPlayer().getHealth() < 0) {
      console.log("Death screen");
    } else {
      this.inputManager.tick();
      this.drawScene();
      this.drawHitbox();
      this.reqAnimationFrameRef = requestAnimationFrame(() => this.tick());
    }
  }

  start() {
    this.reqAnimationFrameRef = requestAnimationFrame(() => this.tick());
  }

  end() {
    cancelAnimationFrame(this.reqAnimationFrameRef);
  }
}
