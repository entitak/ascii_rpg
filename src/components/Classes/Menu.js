import settings from "../Settings/settings.json";
import { getFontSize } from "../utils";

class Menu {
  constructor(setting) {
    this.setting = setting;
    this.width = setting.width;
    this.height = setting.height;
    this.mainCtx = setting.mainCtx;
    this.hitboxCtx = setting.hitboxCtx;
    this.open = false;
  }

  draw() {
    let text = "Paused";
    let fontSize = getFontSize() + 80;
    this.mainCtx.fillStyle = "#F5F5F5";
    this.mainCtx.font = fontSize + "px " + settings.fontFamily;
    this.mainCtx.textBaseline = "hanging";
    let left =
      (this.width * this.setting.pixelWidth -
        this.mainCtx.measureText(text).width) /
      2;
    let right = (this.height * this.setting.pixelHeight - fontSize) / 2;
    this.mainCtx.fillText("Paused", left, right);

    this.mainCtx.beginPath();
    this.mainCtx.lineWidth = "6";
    this.mainCtx.strokeStyle = "#F5F5F5";
    this.mainCtx.rect(
      0,
      0,
      this.width * this.setting.pixelWidth,
      this.height * this.setting.pixelHeight
    );
    this.mainCtx.stroke();
  }
}

export default Menu;
