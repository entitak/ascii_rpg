import settings from "../Settings/settings.json";
import inventorySetting from "../Settings/inventory.json";
import {
  getFontSize,
  getNumberFromString,
  getRandomColor,
  isFloat,
} from "../utils";

class Inventory {
  constructor(setting, maps) {
    this.setting = setting;
    this.width = setting.width;
    this.height = setting.height;
    this.mainCtx = setting.mainCtx;
    this.hitboxCtx = setting.hitboxCtx;
    this.maps = maps;
    this.open = false;
    this.inventorySetting = inventorySetting;
    this.colors = new Set();
    this.hitbox = this.createHitbox();
    this.selected = null;
    this.highlighted = [];
  }

  createHitbox() {
    let firstSlots = this.createHitboxObj(
      this.inventorySetting.inventoryAndHotbar.hotbar.slotRowNum,
      "hotbar"
    );
    let secondSlots = this.createHitboxObj(
      this.inventorySetting.inventoryAndHotbar.inventory.slotColNum *
        this.inventorySetting.inventoryAndHotbar.inventory.slotRowNum,
      "inventory"
    );
    return {
      equipAndStats: {
        equip: this.createHitboxObj(
          this.inventorySetting.equipAndStats.equip.slotColNum * 2 +
            this.inventorySetting.equipAndStats.equip.slotRowNum,
          "equip"
        ),
      },
      actions: this.createHitboxObj(
        this.inventorySetting.actions.actionColNum,
        "actions"
      ),
      inventoryAndHotbar: {
        hotbar: firstSlots,
        inventory: secondSlots,
      },
    };
  }

  createHitboxObj(nums, what) {
    let obj = {};
    for (let i = 0; i < nums; i++) {
      let newColor = getRandomColor();
      while (this.colors.has(newColor)) {
        newColor = getRandomColor();
      }
      this.colors.add(newColor);
      obj["slot" + (this.colors.size - 1)] = {
        color: newColor,
        what: what,
        name: "slot" + (this.colors.size - 1),
      };
    }
    return obj;
  }

  drawHitboxes() {
    this.drawEquipHitboxes(
      this.hitbox.equipAndStats.equip,
      this.inventorySetting.equipAndStats.equip,
      this.inventorySetting.equipAndStats
    );
    this.drawSlotsHitbox(this.hitbox.actions, this.inventorySetting.actions);
    this.drawSlotsHitbox(
      this.hitbox.inventoryAndHotbar.hotbar,
      this.inventorySetting.inventoryAndHotbar.hotbar,
      this.inventorySetting.inventoryAndHotbar
    );
    this.drawSlotsHitbox(
      this.hitbox.inventoryAndHotbar.inventory,
      this.inventorySetting.inventoryAndHotbar.inventory,
      this.inventorySetting.inventoryAndHotbar
    );
  }

  drawEquipHitboxes(hitbox, draw, parent) {
    let x = draw.x + parent.title?.titleOffsetX;
    let y = draw.y + parent.title?.titleOffsetY;
    if (draw.centerX)
      x += this.getCenterX(draw.width, draw.slotWidth, draw.slotRowNum);
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    for (let i = 0; i < draw.slotRowNum; i++) {
      this.drawHitbox(
        hitbox[Object.keys(hitbox)[i]].color,
        x + 0.5 + i * (draw.slotWidth + 1),
        y + 0.375,
        draw.slotWidth + 1,
        draw.slotHeight + 1
      );
    }
    let newY = draw.y + draw.height - draw.slotHeight - 2;
    let leftX = draw.x + parent.title?.titleOffsetX;
    let rightX =
      draw.x + parent.title?.titleOffsetX + draw.width - draw.slotWidth - 2;
    if (parent) {
      leftX += parent.x;
      rightX += parent.x;
      newY += parent.y;
    }
    for (let double = 0; double < 2; double++) {
      for (let o = 0; o < draw.slotColNum; o++) {
        this.drawHitbox(
          hitbox[
            Object.keys(hitbox)[
              draw.slotRowNum + (draw.slotColNum * double + o)
            ]
          ].color,
          double === 0 ? leftX + 0.5 : rightX + 0.5,
          newY + 0.375 - (draw.slotHeight + 1) * o,
          draw.slotWidth + 1,
          draw.slotHeight + 1
        );
      }
    }
  }

  drawSlotsHitbox(hitbox, draw, parent) {
    let cols = draw.slotColNum || draw.actionColNum || 1;
    let rows = draw.slotRowNum || draw.actionRowNum || 1;
    let entityWidth = draw.slotWidth || draw.actionWidth;
    let entityHeight = draw.slotHeight || draw.actionHeight;
    let x = draw.x + draw.title.titleOffsetX;
    let y = draw.y + draw.title.titleOffsetY;
    if (draw.centerX) x += this.getCenterX(draw.width, entityWidth, rows);
    if (draw.centerY)
      y += this.getCenterY(
        draw.height - draw.title.titleOffsetY,
        entityHeight,
        cols
      );
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    for (let o = 0; o < cols; o++) {
      for (let i = 0; i < rows; i++) {
        this.drawHitbox(
          hitbox[Object.keys(hitbox)[rows * o + i]].color,
          x + 0.5 + i * (entityWidth + 1),
          y + 0.375 + o * (entityHeight + 1),
          entityWidth + 1,
          entityHeight + 1
        );
      }
    }
  }

  drawHitbox(color, x, y, width, height) {
    this.hitboxCtx.fillStyle = color;
    this.hitboxCtx.fillRect(
      x * this.setting.pixelWidth,
      y * this.setting.pixelHeight,
      width * this.setting.pixelWidth,
      height * this.setting.pixelHeight
    );
  }

  isClickable(found) {
    if (found.what === "equip") {
      let index = this.maps
        .getPlayer()
        .equip.map((obj) => obj.slot)
        .indexOf(found.name);
      if (this.maps.getPlayer().equip[index].item) return true;
      if (this.highlighted.includes(found.name)) return true;
    } else if (found.what === "inventory" || found.what === "hotbar") {
      let index = this.maps
        .getPlayer()
        .inventory.map((obj) => obj.slot)
        .indexOf(found.name);
      if (this.maps.getPlayer().inventory[index]) return true;
    } else if (found.what === "actions") {
      let equipSlots =
        inventorySetting.equipAndStats.equip.slotRowNum +
        inventorySetting.equipAndStats.equip.slotColNum * 2;
      let inventorySlots =
        inventorySetting.inventoryAndHotbar.hotbar.slotRowNum +
        inventorySetting.inventoryAndHotbar.inventory.slotColNum *
          inventorySetting.inventoryAndHotbar.inventory.slotRowNum;
      let actionsStartIndex =
        getNumberFromString(found.name) - (equipSlots + inventorySlots);
      let action = inventorySetting.actions.names[actionsStartIndex];
      if (this.isActionAvailable(action)) return true;
    }
    return false;
  }

  arrayMove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
  }

  getItemFromObj(clicked) {
    const lastHighlighted = this.highlighted;
    this.highlighted = [];
    if (
      clicked.what === "hotbar" ||
      clicked.what === "inventory" ||
      clicked.what === "equip"
    ) {
      let item;
      if (clicked.what === "equip")
        item = this.getItemFromKeyFromEquip("slot", clicked.name);
      else item = this.getItemFromKeyFromInventory("slot", clicked.name);
      if (
        lastHighlighted.length > 0 &&
        lastHighlighted.includes(clicked.name)
      ) {
        const moveIndex = this.selected.availableSlots.indexOf(
          this.findSlotFromName(
            this.maps.getPlayer().equip,
            "slot",
            clicked.name
          ).name
        );
        this.arrayMove(this.selected.availableSlots, moveIndex, 0);
        this.selected = this.selected.use("EQUIP", this.maps);
      }
      if (!item) return;
      let slotNum = getNumberFromString(item.slot);
      if (this.selected && getNumberFromString(this.selected.slot) === slotNum)
        this.selected = null;
      else {
        this.selected = item;
      }
    } else if (clicked.what === "actions") {
      if (!this.selected) return;
      let equipSlots =
        inventorySetting.equipAndStats.equip.slotRowNum +
        inventorySetting.equipAndStats.equip.slotColNum * 2;
      let inventorySlots =
        inventorySetting.inventoryAndHotbar.hotbar.slotRowNum +
        inventorySetting.inventoryAndHotbar.inventory.slotColNum *
          inventorySetting.inventoryAndHotbar.inventory.slotRowNum;
      let actionsStartIndex =
        getNumberFromString(clicked.name) - (equipSlots + inventorySlots);
      let action = inventorySetting.actions.names[actionsStartIndex];
      if (!this.isActionAvailable(action)) return;
      if (
        this.selected &&
        action === "EQUIP" &&
        this.selected?.availableSlots?.length > 1
      ) {
        this.selected?.availableSlots.forEach((slot) => {
          this.addHighlight(
            this.findSlotFromName(this.maps.getPlayer().equip, "name", slot)
              .slot
          );
        });
      } else {
        this.selected = this.selected.use(action, this.maps);
      }
    }
  }

  findSlotFromName(array, key, prop) {
    let slot;
    array.forEach((arraySlot) => {
      if (arraySlot[key] === prop) slot = arraySlot;
    });
    return slot;
  }

  addHighlight(slot) {
    if (this.highlighted.includes(slot)) return;
    this.highlighted.push(slot);
  }

  removeHighlight(slot) {
    const index = this.highlighted.indexOf(slot);
    if (index === -1) return;
    this.highlighted.splice(index, 1);
  }

  getItemFromKeyFromEquip(key, prop) {
    let found = this.maps.getPlayer().equip.find((obj) => {
      return obj[key] === prop;
    });
    return found?.item;
  }

  getItemFromKeyFromInventory(key, prop) {
    let found = this.maps.getPlayer().inventory.find((obj) => {
      return obj[key] === prop;
    });
    return found;
  }

  draw() {
    this.drawInventoryFrame();
    this.drawEquipAndStats();
    this.drawInventoryAndHotbar();
    this.drawActions();
    this.drawHighlightedSlot("yellow");
    this.highlighted.forEach((slot) => {
      this.drawHighlightedSlot("yellow", { slot: slot });
    });
  }

  drawHighlightedSlot(color, slot = this.selected) {
    if (slot === undefined || slot === null) return;
    slot = getNumberFromString(slot.slot);
    let draw;
    let parent;
    let nth;
    let x = 0;
    let y = 0;
    let hotbarSlots = inventorySetting.inventoryAndHotbar.hotbar.slotRowNum;
    let inventorySlots =
      inventorySetting.inventoryAndHotbar.inventory.slotColNum *
      inventorySetting.inventoryAndHotbar.inventory.slotRowNum;
    if (slot < hotbarSlots) {
      // hotbar
      nth = slot;
      draw = inventorySetting.inventoryAndHotbar.hotbar;
      parent = inventorySetting.inventoryAndHotbar;
      x += (inventorySetting.inventoryAndHotbar.hotbar.slotWidth + 1) * nth;
    } else if (slot < inventorySlots + hotbarSlots) {
      // inventory
      nth = slot - hotbarSlots;
      draw = inventorySetting.inventoryAndHotbar.inventory;
      parent = inventorySetting.inventoryAndHotbar;
      let slotWidth = draw.slotWidth;
      let slotHeight = draw.slotHeight;
      let slotRowNum = draw.slotRowNum;
      x += (nth % slotRowNum) * (slotWidth + 1);
      y += Math.ceil((nth + 1) / slotRowNum - 1) * (slotHeight + 1);
    } else {
      // equip
      nth = slot - hotbarSlots - inventorySlots;
      draw = inventorySetting.equipAndStats.equip;
      parent = inventorySetting.equipAndStats;
      if (nth < draw.slotRowNum) x += (draw.slotWidth + 1) * nth;
      else {
        nth -= draw.slotRowNum;
        x -= this.getCenterX(draw.width, draw.slotWidth, draw.slotRowNum);
        y += draw.height - draw.slotHeight - 2 - parent.title?.titleOffsetY;
        if (nth >= draw.slotColNum) {
          nth -= draw.slotColNum;
          x += draw.width - draw.slotWidth - 2;
        }
        y -= (draw.slotHeight + 1) * nth;
      }
    }
    x += draw.x + (draw.title?.titleOffsetX || parent.title?.titleOffsetX || 0);
    y += draw.y + (draw.title?.titleOffsetY || parent.title?.titleOffsetY || 0);
    if (draw.centerY)
      y += this.getCenterY(
        draw.height - (draw.title.titleOffsetY || 0),
        draw.slotHeight,
        draw.slotColNum
      );
    if (draw.centerX)
      x += this.getCenterX(draw.width, draw.slotWidth, draw.slotRowNum);
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    this.drawBorder(color, x, y, draw.slotWidth + 2, draw.slotHeight + 2);
  }

  drawActions() {
    this.preBorder(
      this.inventorySetting.mainColor,
      this.inventorySetting.actions
    );
    this.preTitle(
      this.inventorySetting.mainColor,
      this.inventorySetting.actions
    );
    this.preSlot(
      this.inventorySetting.mainColor,
      this.inventorySetting.placeholderColor,
      this.inventorySetting.actions
    );
  }

  drawEquipAndStats() {
    this.preTitle(
      this.inventorySetting.mainColor,
      this.inventorySetting.equipAndStats
    );
    this.preStat(
      this.inventorySetting.mainColor,
      this.inventorySetting.statsColor,
      this.inventorySetting.equipAndStats.stats,
      this.inventorySetting.equipAndStats
    );
    this.preEquip(
      this.inventorySetting.mainColor,
      this.inventorySetting.placeholderColor,
      this.inventorySetting.equipAndStats.equip,
      this.inventorySetting.equipAndStats
    );
  }

  drawInventoryAndHotbar() {
    this.preTitle(
      this.inventorySetting.mainColor,
      this.inventorySetting.inventoryAndHotbar.hotbar,
      this.inventorySetting.inventoryAndHotbar
    );
    this.preTitle(
      this.inventorySetting.mainColor,
      this.inventorySetting.inventoryAndHotbar.inventory,
      this.inventorySetting.inventoryAndHotbar
    );
    this.preSlot(
      this.inventorySetting.mainColor,
      this.inventorySetting.placeholderColor,
      this.inventorySetting.inventoryAndHotbar.hotbar,
      this.inventorySetting.inventoryAndHotbar
    );
    this.preSlot(
      this.inventorySetting.mainColor,
      this.inventorySetting.placeholderColor,
      this.inventorySetting.inventoryAndHotbar.inventory,
      this.inventorySetting.inventoryAndHotbar
    );
  }

  preStat(color, placeHolderColor, draw, parent) {
    let names = draw.names;
    while (names.length < draw.statColNum * draw.statRowNum) {
      names.push("");
    }
    let newWidth = draw.statWidth;
    if (draw.statAutoWidth)
      newWidth = Math.round((draw.width - 2) / draw.statColNum);
    let x = draw.x + (draw.title?.titleOffsetX || 0);
    let y = draw.y + (draw.title?.titleOffsetY || 0);
    if (draw.centerY) {
      y += this.getCenterY(
        draw.height - (draw.title.titleOffsetY || 0),
        draw.statHeight,
        draw.statColNum
      );
    }
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    for (let o = 0; o < draw.statRowNum; o++) {
      for (let i = 0; i < draw.statColNum; i++) {
        this.drawBorder(
          color,
          x + newWidth * i,
          y + o * draw.statHeight + o * 2 - o,
          newWidth + 1,
          draw.statHeight + 2
        );
        this.drawSlot(
          placeHolderColor,
          null,
          names[o * draw.statColNum + i],
          x + newWidth * i,
          y + o * draw.statHeight + o * 2 - o,
          newWidth - 1,
          draw.statHeight,
          draw.statCenter,
          true
        );
      }
    }
  }

  preEquip(color, placeHolderColor, draw, parent) {
    let names = draw.names;
    while (names.length < draw.slotColNum * draw.slotRowNum) {
      names.push("");
    }
    let x = draw.x + parent.title?.titleOffsetX;
    let y = draw.y + parent.title?.titleOffsetY;
    if (draw.centerX)
      x += this.getCenterX(draw.width, draw.slotWidth, draw.slotRowNum);
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    for (let i = 0; i < draw.slotRowNum; i++) {
      this.drawBorder(
        color,
        x + (draw.slotWidth + 1) * i,
        y,
        draw.slotWidth + 2,
        draw.slotHeight + 2
      );
      this.drawSlot(
        placeHolderColor,
        this.getItemFromKeyFromEquip("name", names[i]),
        names[i],
        x + (draw.slotWidth + 1) * i,
        y,
        draw.slotWidth,
        draw.slotHeight,
        draw.slotCenter
      );
    }
    let newY = draw.y + draw.height - draw.slotHeight - 2;
    let leftX = draw.x + parent.title?.titleOffsetX;
    let rightX =
      draw.x + parent.title?.titleOffsetX + draw.width - draw.slotWidth - 2;
    if (parent) {
      leftX += parent.x;
      rightX += parent.x;
      newY += parent.y;
    }
    for (let double = 0; double < 2; double++) {
      for (let o = 0; o < draw.slotColNum; o++) {
        this.drawBorder(
          color,
          double === 0 ? leftX : rightX,
          newY - (draw.slotHeight + 1) * o,
          draw.slotWidth + 2,
          draw.slotHeight + 2
        );
        this.drawSlot(
          placeHolderColor,
          this.getItemFromKeyFromEquip(
            "name",
            names[draw.slotRowNum + (draw.slotColNum * double + o)]
          ),
          names[draw.slotRowNum + (draw.slotColNum * double + o)],
          double === 0 ? leftX : rightX,
          newY - (draw.slotHeight + 1) * o,
          draw.slotWidth,
          draw.slotHeight,
          draw.slotCenter
        );
      }
    }
  }

  isActionAvailable(action) {
    if (!this.selected) return;
    let slotNum = getNumberFromString(this.selected.slot);
    let hotbarSlots = inventorySetting.inventoryAndHotbar.hotbar.slotRowNum;
    let inventorySlots =
      inventorySetting.inventoryAndHotbar.inventory.slotColNum *
      inventorySetting.inventoryAndHotbar.inventory.slotRowNum;
    let inInventory = hotbarSlots + inventorySlots;
    switch (this.selected.type) {
      case "potion":
        if (action === "USE" && slotNum < inInventory) return true;
        return false;
      case "attack":
        if (action === "EQUIP" && slotNum < inInventory) return true;
        if (action === "UNEQUIP" && slotNum >= inInventory) return true;
        return false;
      case "defence":
        if (action === "EQUIP" && slotNum < inInventory) return true;
        if (action === "UNEQUIP" && slotNum >= inInventory) return true;
        return false;
      default:
        break;
    }
  }

  preSlot(color, placeHolderColor, draw, parent) {
    let names = draw.names;
    let cols = draw.statRowNum || draw.slotColNum || draw.actionColNum || 1;
    let rows = draw.statColNum || draw.slotRowNum || draw.actionRowNum || 1;
    let entityWidth = draw.slotWidth || draw.actionWidth || draw.statWidth;
    let entityHeight = draw.slotHeight || draw.actionHeight || draw.statHeight;
    let entityCenter = draw.slotCenter || draw.actionCenter;
    while (names.length < cols * rows) {
      names.push("");
    }
    if (draw.statAutoWidth) entityWidth = Math.round((draw.width - 2) / cols);
    let x = draw.x + (draw.title?.titleOffsetX || 0);
    let y = draw.y + (draw.title?.titleOffsetY || 0);
    if (draw.centerY)
      y += this.getCenterY(
        draw.height - (draw.title.titleOffsetY || 0),
        entityHeight,
        cols
      );
    if (draw.centerX)
      x += this.getCenterX(draw.width, draw.slotWidth, draw.slotRowNum);
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    let actionColor = placeHolderColor;
    for (let o = 0; o < cols; o++) {
      for (let i = 0; i < rows; i++) {
        let slotIndex = -1;
        if (parent?.inventory?.isInventory) {
          slotIndex = o * rows + i;
          if (draw.isInventory) {
            slotIndex += parent.hotbar.slotRowNum;
          }
        }
        if (
          names[o * rows + i] === "USE" ||
          names[o * rows + i] === "EQUIP" ||
          names[o * rows + i] === "UNEQUIP"
        ) {
          if (this.isActionAvailable(names[o * rows + i])) actionColor = color;
          else actionColor = placeHolderColor;
        }
        this.drawBorder(
          color,
          x + (entityWidth + 1) * i,
          y + o * entityHeight + o * 2 - o,
          entityWidth + 2,
          entityHeight + 2
        );
        this.drawSlot(
          actionColor,
          slotIndex !== -1
            ? this.getItemFromKeyFromInventory("slot", "slot" + slotIndex)
            : null,
          names[o * rows + i],
          x + (entityWidth + 1) * i,
          y + o * entityHeight + o * 2 - o,
          entityWidth,
          entityHeight,
          entityCenter,
          draw.statWidth ? true : false
        );
      }
    }
  }

  drawInventoryFrame() {
    this.drawBorder(
      this.inventorySetting.mainColor,
      0,
      0,
      this.width,
      this.height,
      this.mainFrameConditions
    );
  }

  preBorder(color, draw, parent) {
    let x = draw.x;
    let y = draw.y;
    let width = draw.width;
    let height = draw.height;
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    this.drawBorder(color, x, y, width, height);
  }

  preTitle(color, draw, parent) {
    let x = draw.x;
    let y = draw.y + draw.title.y;
    if (draw.title.centerX) {
      x += this.getTextCenterX(draw.width, draw.title.text);
    } else {
      x += draw.title.x;
    }
    if (parent) {
      x += parent.x;
      y += parent.y;
    }
    this.drawTitle(color, draw.title.text, x, y);
  }

  getTextCenterX(width, text) {
    return Math.round(width / 2 - text.length / 2);
  }

  getTextCenterY(height, textHeight) {
    return Math.round((height - textHeight) / 2);
  }

  getCenterX(fullWidth, width, nums = 0) {
    return Math.round(fullWidth / 2 - (width * nums + nums + 1) * 0.5);
  }

  getCenterY(fullHeight, height, nums = 1) {
    return Math.round(fullHeight / 2 - Math.ceil(((height + 2) * nums) / 2));
  }

  transformToStatText(text, width) {
    let result = [];
    if (text.length !== 0) {
      let newText = text.split(0, -1) + ":";
      let stat;
      if (newText.length + 6 > width) {
        if (text === "Health") newText = "HP:";
        else if (text === "Damage") newText = "DMG:";
        else if (text === "Defence") newText = "DEF:";
      }
      if (text === "Health") stat = this.maps.getPlayer().getHealth();
      else if (text === "Damage") stat = this.maps.getPlayer().getDamage();
      else if (text === "Defence") stat = this.maps.getPlayer().getArmor();
      else return [text];
      let abb = ["", "k", "m", "b"];
      let i = 0;
      let num = stat;
      while (~~((num / 1000) % 1000) !== 0) {
        num = num / 1000;
        i++;
      }
      const decimalPlaces =
        stat >= 0
          ? parseInt(num).toString().length
          : parseInt(num).toString().length - 1;
      if (isFloat(num)) stat = num.toFixed(3 - decimalPlaces);
      stat += abb[i];
      stat = " ".repeat(width - newText.length - stat.toString().length) + stat;
      newText += stat;
      result.push(newText);
    }
    return result;
  }

  splitToWords(text, width) {
    let result = [];
    let words = text.split(" ");
    let rowText = "";
    let i = 0;
    while (i < words.length) {
      let word = words[i];
      if (word.length > width) {
        word = word.slice(0, -(word.length - width + 1)) + "…";
        if (rowText.length > 0) result.push(rowText);
        result.push(word);
        rowText = "";
      } else {
        if (rowText.length + word.length + 1 > width) {
          if (rowText.length > 0) result.push(rowText);
          result.push(word);
          rowText = "";
        } else {
          rowText += rowText.length > 0 ? " " + word : word;
        }
      }
      if (i + 1 === words.length && rowText.length > 0) result.push(rowText);
      i++;
    }
    return result;
  }

  drawSlot(color, item, text, x, y, width, height, center, stat) {
    if (!item) {
      let newY = y + 1;
      let words;
      if (stat) words = this.transformToStatText(text, width);
      else words = this.splitToWords(text, width);
      if (center)
        newY += this.getTextCenterY(height - 1, Math.min(words.length, height));
      for (
        let textRowIndex = 0;
        textRowIndex < Math.min(words.length, height);
        textRowIndex++
      ) {
        for (let i = 0; i < words[textRowIndex].length; i++) {
          let newX = x + i + 1;
          if (center)
            newX += this.getTextCenterX(width - 1, words[textRowIndex]);
          this.drawChar(words[textRowIndex][i], color, newX, newY);
        }
        newY++;
      }
    } else {
      this.drawItem(item, width, height, x, y);
    }
  }

  drawItem(item, width, height, x, y) {
    x += this.getCenterX(width, item.data[0].length);
    y += this.getCenterY(height, item.data.length - 2);
    for (let o = 0; o < item.data.length; o++) {
      for (let i = 0; i < item.data[o].length; i++) {
        this.drawChar(item.data[o][i], item.color[o][i], x + 1 + i, y + 1 + o);
      }
    }
  }

  drawBorder(color, x, y, width, height, conditionFn = this.defaultBorder) {
    for (let row = 0; row < height; row++) {
      for (let col = 0; col < width; col++) {
        let char = conditionFn(col, row, width, height);
        if (char) this.drawChar(char, color, x + col, y + row);
      }
    }
  }

  defaultBorder(col, row, width, height) {
    let char;
    if (col === 0 && row === 0) char = "╔";
    else if (col === 0 && row === height - 1) char = "╚";
    else if (col === width - 1 && row === 0) char = "╗";
    else if (col === width - 1 && row === height - 1) char = "╝";
    else if (col === 0 || col === width - 1) char = "║";
    else if (row === 0 || row === height - 1) char = "═";
    return char;
  }

  mainFrameConditions(col, row, width, height) {
    let char;
    // Left top corner
    if (col === 2 && row === 0) char = "╒";
    else if (col === 2 && row === 1) char = "╛";
    else if (col === 1 && row === 1) char = "╒";
    else if (col === 1 && row === 2) char = "╛";
    else if (col === 0 && row === 2) char = "╔";
    // Right top corner
    else if (col === width - 3 && row === 0) char = "╕";
    else if (col === width - 3 && row === 1) char = "╘";
    else if (col === width - 2 && row === 1) char = "╕";
    else if (col === width - 2 && row === 2) char = "╘";
    else if (col === width - 1 && row === 2) char = "╗";
    // Left bottom corner
    else if (col === 0 && row === height - 3) char = "╚";
    else if (col === 1 && row === height - 3) char = "╕";
    else if (col === 1 && row === height - 2) char = "╘";
    else if (col === 2 && row === height - 2) char = "╕";
    else if (col === 2 && row === height - 1) char = "╘";
    // Right bottom corner
    else if (col === width - 1 && row === height - 3) char = "╝";
    else if (col === width - 2 && row === height - 3) char = "╒";
    else if (col === width - 2 && row === height - 2) char = "╛";
    else if (col === width - 3 && row === height - 2) char = "╒";
    else if (col === width - 3 && row === height - 1) char = "╛";
    // Other
    else if (
      (col === 0 && row < height - 3 && row > 2) ||
      (col === width - 1 && row < height - 3 && row > 2)
    )
      char = "║";
    else if (
      (row === 0 && col < width - 3 && col > 2) ||
      (row === height - 1 && col < width - 3 && col > 2)
    )
      char = "═";
    return char;
  }

  drawTitle(color, text, x, y) {
    for (let char = 0; char < text.length; char++) {
      this.drawChar(text[char], color, x + char, y);
    }
  }

  drawChar(char, color, x, y) {
    this.mainCtx.fillStyle = color;
    this.mainCtx.font = getFontSize() + "px " + settings.fontFamily;
    this.mainCtx.textBaseline = "hanging";
    this.mainCtx.fillText(
      char,
      x * this.setting.pixelWidth,
      y * this.setting.pixelHeight,
      this.setting.pixelWidth
    );
  }
}

export default Inventory;
