import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        margin     : 0;
        padding    : 0;
        box-sizing : border-box;
        overflow: hidden;
        user-select: none;
    }

    #root {
        min-height: 100vh;
        min-width: 100vw;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    button {
        cursor: pointer;
        outline: none;
    }

    input {
        outline: none;
    }
`;
