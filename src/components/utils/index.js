import setting from "../Settings/settings.json";

export const isPercent = (percent) =>
  Math.random() * 100 < percent ? true : false;

export const isFloat = (n) => n === +n && n !== (n | 0);

export const rgbToHex = (r, g, b) => {
  let hex = ((r << 16) | (g << 8) | b).toString(16);
  return "#" + "0".repeat(6 - hex.length) + hex;
};

export const getRandomColor = () =>
  "#" + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, "0");

export const getFontSize = () => {
  let fontSize = setting.fontSize;
  if (window.innerWidth < 1560)
    fontSize -= (14 - ~~(window.innerWidth / 104)) * 2;
  return fontSize;
};

export const getRandomInt = (max) =>
  Math.floor(Math.random() * Math.floor(max));

export const getNumberFromString = (string) =>
  parseInt(string.replace(/\D/g, ""));

export const findObjByPropInNested = (obj, key, prop) => {
  if (obj[key] === prop) {
    return obj;
  }
  var result, i;
  for (i in obj) {
    if (obj.hasOwnProperty(i) && typeof obj[i] === "object") {
      result = findObjByPropInNested(obj[i], key, prop);
      if (result) {
        return result;
      }
    }
  }
  return result;
};

export const getColFromIndex = (index) => index % setting.gameSize.width;

export const getRowFromIndex = (index) => ~~(index / setting.gameSize.width);
