import styled from 'styled-components';

export const ContainerStyled = styled.div`
    display: flex;
    flex-direction: row;
    min-height: 100vh;
    min-width: 100vw;
    align-items: center;
    justify-content: center;
    background-color: slategray;
    overflow: hidden;
    position: relative;

    button {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    button i {
        overflow: visible;
    }
`;

export const ControlStyled = styled.div<any>`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: slategrey;
    padding: 1rem 1rem;
    gap: 4px;
    width: 300px;
    height: ${props => props.fullHeight + 'px'};
`;

export const CopyStyled = styled.button`
    padding: 1.8rem 1rem;
    width: 100%;
    border-radius: 8px;
    border: none;
    background-color: lightslategray;
    color: whitesmoke;
    box-shadow: inset 0 0 5px skyblue;
    font-size: 21px;
    font-family: cursive;
    transition: .6s ease;

    &:hover {
        color: skyblue;
        box-shadow: inset 0 0 10px skyblue;
    }
`;

export const ResetStyled = styled(CopyStyled)`
    padding: 1.8rem 0;
`;

export const ShowAnimationsStyled = styled(CopyStyled)`
    padding: 1.8rem 0;
`;

export const ColorInput = styled.input``;
export const ColorLabel = styled.label<any>`
    place-self: center;
    padding-left: 8px;
    font-family: cursive;
    color: ${props => props.color};
    text-shadow: 1px 1px 2px black;
`;

export const ColorStyled = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    width: 100%;
    padding: .6rem 0;

    &:hover ${ColorInput}, &:hover ${ColorLabel} {
        filter: opacity(.6);
        cursor: pointer;
    }
`;

export const GameStyled = styled.div<any>`
    display: flex;
    flex-wrap: wrap;
    width: ${props => props.fullWidth + 'px'};
    height: ${props => props.fullHeight + 'px'};
    background-color: black;
    position: relative;
    cursor: none;

    textarea:hover {
        filter: brightness(.6);
    }
`;

export const AnimationContainer = styled.div`
    display: flex;
    flex-direction: column;
    gap: 6px;
`;

export const PlaybackContainer = styled.div`
    display: flex;
    justify-content: center;
`;

export const AnimationPlayStyled = styled.button`
    padding: .6rem 1rem;
    border: none;
    background-color: lightslategray;
    color: whitesmoke;
    box-shadow: inset 0 0 5px skyblue;
    font-size: 18px;
    font-family: cursive;
    transition: color .6s ease, box-shadow .2s ease;
    line-height: 16px;
    overflow: visible;
    width: 50px;

    &:hover {
        color: skyblue;
        box-shadow: inset 0 0 10px white;
    }
`;

export const AnimationLeftStyled = styled(AnimationPlayStyled)`
    border-radius: 8px 0 0 8px;
    width: auto;
`;
export const AnimationRightStyled = styled(AnimationPlayStyled)`
    border-radius: 0 8px 8px 0;
    width: auto;
`;

export const AnimationDuplicateStyled = styled(AnimationLeftStyled)`
    border-radius: 0;
    padding: .6rem .8rem;
`;

export const AnimationSaveStyled = styled(AnimationDuplicateStyled)`
    border-radius: 8px;
    padding: .6rem .8rem;
`;

export const AnimationDeleteStyled = styled(AnimationRightStyled)`
    border-radius: 0 8px 8px 0;

`;
export const AnimationNewStyled = styled(AnimationPlayStyled)`
    border-radius: 8px 0 0 8px;
    width: auto;
`;

export const AnimationBtnContainer = styled.div`
    display:flex;
`;

export const AnimationFrameStyled = styled.h3`
    color: lightgray;
    font-size: 16px;
    place-self: center;
`;

export const GhostingCheckboxStyled = styled.input`
    pointer-events: none;
`;

export const GhostingOpacitySpan = styled.span`
    color: darkgray;
    font-size: 13px;
    place-self: center;
`;

export const GhostingOpacityStyled = styled.input``;

export const GhostingStyled = styled.label<any>`
    justify-content: center;
    color: lightgray;
    font-size: 16px;
    display: flex;
    align-items: center;
    flex-direction: revert;
    gap: 8px;
    cursor: pointer;
`;

export const AnimationImportStyled = styled(CopyStyled)``;

export const ImportContainerStyled = styled.div`
    position: fixed;
    height: 100vh;
    width: 100vw;
    top: 0;
    left: 0;
    backdrop-filter: blur(4px) brightness(.7);
    z-index: 9999;
`;

export const ImportInnerContainerStyled = styled.div`
    position: relative;
    height: 50vh;
    width: 60vw;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: slategrey;
    padding: 1rem;
    border-radius: 6px;
`;

export const ImportTextAreaStyled = styled.textarea`
    width: 100%;
    height: calc(100% - 4rem);
    resize: none;
    outline: none;
    border: none;
    border-radius: 12px 12px 0 0;
    padding: 12px;
    overflow: auto;
`;

export const ImportButtonStyled = styled.button`
    width: 100%;
    border: none;
    background-color: white;
    color: slategray;
    height: 4rem;
    border-radius: 0 0 12px 12px;
    font-size: 35px;
    transition: .2s ease;

    &:hover {
        filter: opacity(.8);
    }
`;

export const AnimationsContainerStyled = styled(ImportContainerStyled)``;
export const AnimationsInnerContainerStyled = styled.div`
    display: flex;
    align-items: baseline;
    justify-content: space-evenly;
    gap: 1rem;
    flex-wrap: wrap;
    padding: 1rem;
    background-color: slategrey;
`;

export const AnimationsCustomScrollbarStyled = styled.div`
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 6px;
    max-height: 80vh;
    max-width: 60vw;
    overflow-y: auto;
`;

export const CanvasStyled = styled.canvas<any>`
    background-color: black;
    cursor: pointer;
    transition: filter .2s ease;
    border-radius: 4px;

    &:hover {
        filter: opacity(.8);
    }
`;

export const Title = styled.h1`
    color: whitesmoke;
`;

export const AnimationShowcaseStyled = styled.div`
    display: flex;
`;
