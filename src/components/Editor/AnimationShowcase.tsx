import { useEffect, useRef, useState } from 'react';
import { getFontSize } from '../utils';
import { CanvasStyled, AnimationContainer, Title } from './style';

export default function AnimationShowcase({ animation, name, editExistingAnimation, index, tileSize, sceneSize }: any) {
    const [animationSelect, setAnimationSelect] = useState<number>(0);
    const [animationFrame, setAnimationFrame] = useState(animation[0]);
    const [showcasePlaying, setShowcasePlaying] = useState<boolean>(false);
    const canvasRef = useRef<HTMLCanvasElement>(null);

    useEffect(() => {
        const animationInterval = setInterval(() => {
            if (showcasePlaying) {
                const newFrame = animationSelect < animation.length - 1 ? animationSelect + 1 : 0;
                setAnimationSelect(animationSelect => newFrame);
                setAnimationFrame((animationFrame: any) => animation[newFrame]);
            }
        }, 250)

        return () => clearInterval(animationInterval);
        // eslint-disable-next-line
    }, [showcasePlaying, animationSelect]);

    useEffect(() => {
        if (!canvasRef.current) return
        const canvasCtx = canvasRef.current.getContext('2d');
        canvasRef.current.width = tileSize[0] * sceneSize[0];
        canvasRef.current.height = tileSize[1] * sceneSize[1];

        if (!canvasCtx) return
        // eslint-disable-next-line
        animationFrame.map((pixel: any) => {
            canvasCtx.textBaseline = "hanging";
            canvasCtx.font = getFontSize() + 'px monospace';
            canvasCtx.fillStyle = pixel.color;
            canvasCtx.fillText((pixel.char).slice(0, 1), pixel.left * tileSize[0], pixel.top * tileSize[1]);
        });
        // eslint-disable-next-line
    })

    return (
        <AnimationContainer>
            <Title>{name}</Title>
            <CanvasStyled ref={canvasRef}
                onClick={() => editExistingAnimation(index)}
                onMouseEnter={() => setShowcasePlaying((showcasePlaying: any) => true)}
                onMouseLeave={() => {
                    setShowcasePlaying((showcasePlaying: any) => false);
                    setAnimationSelect(animationSelect => 0);
                    setAnimationFrame((animationFrame: any) => animation[0]);
                }} />
        </AnimationContainer>
    )
}