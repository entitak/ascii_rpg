import { useState, useEffect, useMemo, useRef } from "react";
import { allAnimations, animationsToInputFormat } from "../Animations/allAnimations";
import {
  ContainerStyled,
  ControlStyled,
  CopyStyled,
  ResetStyled,
  ShowAnimationsStyled,
  ColorStyled,
  ColorInput,
  ColorLabel,
  GameStyled,
  AnimationContainer,
  PlaybackContainer,
  AnimationPlayStyled,
  AnimationLeftStyled,
  AnimationRightStyled,
  AnimationBtnContainer,
  AnimationSaveStyled,
  AnimationDuplicateStyled,
  AnimationNewStyled,
  AnimationDeleteStyled,
  AnimationFrameStyled,
  GhostingCheckboxStyled,
  GhostingOpacitySpan,
  GhostingOpacityStyled,
  GhostingStyled,
  AnimationImportStyled,
  ImportContainerStyled,
  ImportTextAreaStyled,
  ImportInnerContainerStyled,
  ImportButtonStyled,
  AnimationsContainerStyled,
  AnimationsInnerContainerStyled,
  AnimationsCustomScrollbarStyled
} from "./style";
import setting from "./../Settings/settings.json";
import AnimationShowcase from "./AnimationShowcase";
import { getFontSize } from "../utils";

export default function Editor() {
  const sceneSize = useMemo(() => [setting.gameSize.width, setting.gameSize.height], []);
  const [windowSize, setWindowSize] = useState<Array<number>>([
    window.innerWidth - 300,
    window.innerHeight,
  ]);
  const [animation, setAnimation] = useState<Array<Array<any>>>(
    localStorage.getItem("animation")
      ? JSON.parse(localStorage.getItem("animation") || "{}")
      : [[]]
  );
  const [animationFocus, setAnimationFocus] = useState<number>(0);
  const [animationPlaying, setAnimationPlaying] = useState<boolean>(false);
  const [focusedTile, setFocusedTile] = useState<any>({ left: 0, top: 0 });
  const [openImport, setOpenImport] = useState<boolean>(false);
  const [openAnimations, setOpenAnimations] = useState<boolean>(false);
  const [inputColor, setInputColor] = useState<string>(
    localStorage.getItem("inputColor")
      ? JSON.parse(localStorage.getItem("inputColor") || "{}")
      : "#F5F5F5"
  );
  const [ghosting, setGhosting] = useState<boolean>(
    localStorage.getItem("ghosting")
      ? JSON.parse(localStorage.getItem("ghosting") || "{}")
      : true
  );
  const [ghostingOpacity, setGhostingOpacity] = useState<string>(
    localStorage.getItem("ghostingOpacity")
      ? JSON.parse(localStorage.getItem("ghostingOpacity") || "{}")
      : "0.5"
  );
  const tileSize = [
    windowSize[0] / sceneSize[0],
    windowSize[1] / sceneSize[1]
  ];
  const importRef = useRef<HTMLDivElement>(null);
  const textAreaRef = useRef<HTMLTextAreaElement>(null);
  const animationsRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    function moveFocused(e: any) {
      // Change focus using arrows
      function changeFocus(left: number, top: number) {
        if (document.querySelector('.tile[data-col="' + (focusedTile.left + left) + '"][data-row="' + (focusedTile.top + top) + '"]') !== null || focusedTile.left + left < 0 || focusedTile.top + top < 0) return;
        animation[animationFocus].push({
          char: '',
          color: 'white',
          top: focusedTile.top + top,
          left: focusedTile.left + left
        })
        setFocusedTile((previous: any) => ({
          left: previous.left + left,
          top: previous.top + top
        }));
        setAnimation((animation: any) => [...animation])
      }

      switch (e.keyCode) {
        // left arrow
        case 37:
          changeFocus(-1, 0);
          break;
        // up arrow
        case 38:
          changeFocus(0, -1);
          break;
        // right arrow
        case 39:
          changeFocus(1, 0);
          break;
        // down arrow
        case 40:
          changeFocus(0, 1);
          break;
      }
    }

    document.addEventListener("keydown", moveFocused);
    return () => document.removeEventListener("keydown", moveFocused);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [focusedTile]);

  useEffect(() => {
    // Refresh vars after resizing
    function resizeWindow() {
      setWindowSize((windowSize) => [
        window.innerWidth - 300,
        window.innerHeight,
      ]);
    }

    window.addEventListener("resize", resizeWindow);
    return () => window.removeEventListener("resize", resizeWindow);
  }, []);

  useEffect(() => {
    // Animation loop interval => changing animation focus in interval
    const animationInterval = setInterval(() => {
      if (!animationPlaying) return;
      const newIndex =
        animationFocus < animation.length - 1 ? animationFocus + 1 : 0;
      setAnimationFocus(newIndex);
    }, 300);

    return () => clearInterval(animationInterval);
  }, [animationPlaying, animationFocus, animation]);

  useEffect(() => {
    // Autofocus on focusedTile on rerender
    (document.querySelector('.tile[data-col="' + focusedTile.left + '"][data-row="' + focusedTile.top + '"]') as HTMLElement)?.focus()
  })

  function animationToOutputFormat() {
    const newArray: any = [];
    const copyAnimation = JSON.parse(JSON.stringify(animation));
    copyAnimation.forEach((scene: any) => {
      if (scene.length === 0) return;
      let lowestRow = scene[0].top;
      let lowestCol = scene[0].left;
      let highestRow = scene[0].top;
      let highestCol = scene[0].left;
      let newScene = scene;
      newScene.forEach((tile: any) => {
        const tempLowestRow = tile.top;
        const tempLowestCol = tile.left;
        const tempHighestRow = tile.top;
        const tempHighestCol = tile.left;
        if (tempLowestRow < lowestRow) lowestRow = tempLowestRow;
        if (tempLowestCol < lowestCol) lowestCol = tempLowestCol;
        if (tempHighestRow > highestRow) highestRow = tempHighestRow;
        if (tempHighestCol > highestCol) highestCol = tempHighestCol;
      });
      const width = highestCol - lowestCol + 1;
      const height = highestRow - lowestRow + 1;
      newScene.forEach((tile: any) => {
        tile.left -= lowestCol;
        tile.top -= lowestRow;
      });
      newArray.push({
        x: lowestCol,
        y: lowestRow,
        width: width,
        height: height,
        data: newScene
      });
    });
    return newArray
  }

  function copyToClipboard(array: Array<any>) {
    // Copy to clipboard
    const copyArray = animationToOutputFormat();
    var temp = document.createElement("textarea");
    document.body.appendChild(temp);
    temp.value = JSON.stringify(copyArray);
    temp.select();
    document.execCommand("copy");
    document.body.removeChild(temp);
  }

  function createHoverElement(e: any) {
    // Create hover tile on cursor position
    const newIndex = getThisIndex(e);
    var temp = false;
    document.querySelectorAll(".hoverElement").forEach((el) => {
      const left = el.getAttribute("data-col");
      const top = el.getAttribute("data-row");
      if (left === JSON.stringify(newIndex.left) && top === JSON.stringify(newIndex.top)) {
        temp = true;
        return;
      }
      el.remove();
    });

    if (temp || e.target.tagName !== "DIV") {
      return;
    }
    const top = newIndex.top * tileSize[1];
    const left = newIndex.left * tileSize[0];
    const hoverElement = document.createElement("div");
    hoverElement.classList.add("hoverElement");
    hoverElement.setAttribute("data-col", JSON.stringify(newIndex.left));
    hoverElement.setAttribute("data-row", JSON.stringify(newIndex.top));
    hoverElement.style.cssText =
      `
        position: absolute;
        top: ` +
      top +
      `px;
        left: ` +
      left +
      `px;
        background-color: black;
        filter: invert(1);
        width: ` +
      tileSize[0] +
      `px;
        height: ` +
      tileSize[1] +
      `px;
    `;
    e.target.appendChild(hoverElement);
  }

  function getThisIndex(e: any) {
    const left = ~~(e.clientX / tileSize[0]);
    const top = ~~(e.clientY / tileSize[1]);
    return { top: top, left: left };
  }

  function createTextArea(e: any) {
    // If clicked on hover element then create textarea under it
    if (!e.target.classList.contains('hoverElement')) return;
    const newIndex = getThisIndex(e);
    setFocusedTile(newIndex);
    animation[animationFocus].push({
      char: '',
      color: 'white',
      top: newIndex.top,
      left: newIndex.left
    })
    setAnimation((animation: any) => [...animation])
  }

  function changeValueInTextArea(e: any, index: number) {
    // Change value in textarea
    animation[animationFocus][index].char = e.target.value;
    animation[animationFocus][index].color = inputColor;
    setAnimation((animation: any) => [...animation]);
  }

  function removeTextArea(target: any, index: number) {
    // Remove textarea by index if empty on Blur
    if (target.value.length > 0) return
    animation[animationFocus].splice(index, 1);
    setAnimation((animation: any) => [...animation]);
  }

  function editExistingAnimation(index: any) {
    setOpenAnimations(false);
    let existingAnimation = allAnimations[index].animation;
    localStorage.setItem('animation', JSON.stringify(existingAnimation));
    setAnimation(existingAnimation);
    setAnimationPlaying(false)
    setAnimationFocus(0);
    setOpenImport(false);
  }

  function transformToInput() {
    const importedParse = JSON.parse(textAreaRef?.current?.value || '{}');
    const importedAnimation = typeof (importedParse) === 'object' ? importedParse : null;

    if (!importedAnimation) return
    const importAnimation = animationsToInputFormat(importedAnimation);
    localStorage.setItem('animation', JSON.stringify(importAnimation));
    setAnimation(importAnimation);
    setAnimationPlaying(false)
    setAnimationFocus(0);
    setOpenImport(false);
  }

  return (
    <ContainerStyled>
      {openImport && <ImportContainerStyled ref={importRef} onClick={(e: any) => {
        if (importRef?.current?.isEqualNode(e.target)) {
          setOpenImport((openImport: any) => false);
        }
      }} >
        <ImportInnerContainerStyled>
          <ImportTextAreaStyled ref={textAreaRef}
            autoComplete="off" autoCorrect="off" autoCapitalize="off" spellCheck="false" />
          <ImportButtonStyled onClick={() => {
            transformToInput();
          }} >Import</ImportButtonStyled>
        </ImportInnerContainerStyled>
      </ImportContainerStyled>}
      {openAnimations && <AnimationsContainerStyled ref={animationsRef} onClick={(e: any) => {
        if (animationsRef?.current?.isEqualNode(e.target)) {
          setOpenAnimations((openAnimations: any) => false);
        }
      }}>
        <AnimationsCustomScrollbarStyled>
          <AnimationsInnerContainerStyled>
            {allAnimations.map((animation: any, index: number) => {
              return <AnimationShowcase key={index} index={index} sceneSize={sceneSize} tileSize={tileSize} editExistingAnimation={editExistingAnimation} animation={animation.animation} name={animation.name} />
            })}
          </AnimationsInnerContainerStyled>
        </AnimationsCustomScrollbarStyled>
      </AnimationsContainerStyled>}
      <GameStyled
        fullWidth={windowSize[0]}
        fullHeight={windowSize[1]}
        onMouseMove={(e: any) => createHoverElement(e)}
        onClick={(e: any) => createTextArea(e)}
      >
        {animation[animationFocus].map((tile: any, index: number) => {
          // Create textarea from animation
          const topTile = tile.top;
          const leftTile = tile.left;
          const className: string = 'tile';
          const top = topTile * tileSize[1];
          const left = leftTile * tileSize[0];

          return <textarea
            key={index}
            className={className}
            data-col={leftTile}
            data-row={topTile}
            maxLength={1}
            autoComplete="off"
            autoCorrect="off"
            autoCapitalize="off"
            spellCheck="false"

            style={{
              position: "absolute",
              backgroundColor: "black",
              resize: "none",
              border: "none",
              outline: "none",
              width: tileSize[0] + "px",
              height: tileSize[1] + "px",
              color: tile.color,
              top: top + "px",
              left: left + "px",
              fontSize: getFontSize() + 'px',
              lineHeight: tileSize[1] + 'px',
              boxShadow: tile.char.length === 0 ? 'inset 0 0 2px 0 grey' : '',
              zIndex: 2
            }}

            onChange={(e: any) => changeValueInTextArea(e, index)}
            onBlur={(e: any) => removeTextArea(e.target, index)}
            value={tile.char} />
        })}
        {
          animation[animationFocus - 1] && ghosting && animation[animationFocus - 1].map((tile: any, index: number) => {
            // Create textarea from last animation frame => ghosting
            const topTile = tile.top;
            const leftTile = tile.left;
            const className: string = 'tile-ghosting';
            const top = topTile * tileSize[1];
            const left = leftTile * tileSize[0];
            if (document.querySelector('.tile[data-col="' + leftTile + '"][data-row="' + topTile + '"]')) return;
            
            return <textarea
              key={index}
              className={className}
              data-col={leftTile}
              data-row={topTile}
              maxLength={1}
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
              spellCheck="false"

              style={{
                position: "absolute",
                backgroundColor: "black",
                resize: "none",
                border: "none",
                outline: "none",
                width: tileSize[0] + "px",
                height: tileSize[1] + "px",
                color: tile.color,
                top: top + "px",
                left: left + "px",
                opacity: ghostingOpacity,
                fontSize: tileSize[0] + 9 + 'px',
                lineHeight: tileSize[1] + 'px',
                boxShadow: tile.char.length === 0 ? 'inset 0 0 2px 0 grey' : '',
                pointerEvents: "none"
              }}
              onChange={(e: any) => changeValueInTextArea(e, index)}
              onBlur={(e: any) => removeTextArea(e.target, index)}
              value={tile.char} />
          })
        }
      </GameStyled>
      <ControlStyled fullHeight={windowSize[1]}>
        <AnimationContainer>
          <AnimationFrameStyled>Frame: {animationFocus}</AnimationFrameStyled>
          <PlaybackContainer>
            <AnimationLeftStyled onClick={() => {
              const newIndex = animationFocus > 0 ? animationFocus - 1 : 0;
              setAnimationFocus(newIndex);
            }}
            ><i className="fas fa-caret-left" /></AnimationLeftStyled>
            <AnimationPlayStyled
              onClick={() => setAnimationPlaying((animationPlaying: any) => !animationPlaying)}>
              {!animationPlaying ?
                <i className="fas fa-play" />
                :
                <i className="fas fa-pause" />}
            </AnimationPlayStyled>
            <AnimationRightStyled onClick={() => {
              const newIndex = animationFocus < animation.length - 1 ? animationFocus + 1 : animation.length - 1;
              setAnimationFocus(newIndex);
            }}
            ><i className="fas fa-caret-right" /></AnimationRightStyled>
          </PlaybackContainer>
          <GhostingStyled htmlFor="ghosting" >Ghosting:
            <GhostingCheckboxStyled id="ghosting" name="ghosting" type="checkbox" checked={ghosting} onChange={() => {
              setGhosting((ghosting: any) => !ghosting)
              localStorage.setItem('ghosting', JSON.stringify(!ghosting));
            }} />
          </GhostingStyled>
          <GhostingOpacitySpan>Opacity {ghostingOpacity}</GhostingOpacitySpan>
          <GhostingOpacityStyled type="range" name="opacity" min="0" max="1" step="0.1" value={ghostingOpacity}
            onChange={(e): any => {
              setGhostingOpacity((ghostingOpacity: any) => e.target.value);
              localStorage.setItem('ghostingOpacity', JSON.stringify(e.target.value));
            }} />
          <AnimationBtnContainer>
            <AnimationNewStyled onClick={() => {
              setAnimation((animation: any) => [...animation, []]);
              setAnimationFocus(animation.length);
            }} >New</AnimationNewStyled>
            <AnimationDuplicateStyled onClick={() => {
              const noReference = JSON.parse(JSON.stringify(animation[animationFocus]));
              setAnimation((animation: any) => [...animation, noReference]);
              setAnimationFocus(animation.length);
            }}>Duplicate</AnimationDuplicateStyled>
            <AnimationDeleteStyled onClick={() => {
              const newIndex = animation.length > 0 ? animation.length - 1 : 0;
              if (newIndex > 0) {
                animation.splice(animationFocus, 1)
                setAnimation((animation: any) => [...animation]);
                setAnimationFocus(newIndex - 1);
                localStorage.setItem('animation', JSON.stringify(animation));
              } else {
                setAnimation([[]]);
                localStorage.removeItem('animation')
              }
            }} >Delete</AnimationDeleteStyled>
          </AnimationBtnContainer>
          <AnimationSaveStyled onClick={() => {
            localStorage.setItem('animation', JSON.stringify(animation))
          }} >Save</AnimationSaveStyled>
          <AnimationImportStyled onClick={() => setOpenImport(true)} >Import animation</AnimationImportStyled>
        </AnimationContainer>
        <ColorStyled>
          <ColorInput id="color" type="color" name="color" value={inputColor}
            onChange={(e): any => {
              setInputColor(e.target.value);
              localStorage.setItem('inputColor', JSON.stringify(e.target.value));
            }} />
          <ColorLabel htmlFor="color" color={inputColor} >Color</ColorLabel>
        </ColorStyled>
        <ShowAnimationsStyled onClick={() => setOpenAnimations(true)}>Show animations</ShowAnimationsStyled>
        <CopyStyled onClick={() => copyToClipboard(animation)} >Copy to clipboard</CopyStyled>
        <ResetStyled onClick={() => {
          setAnimation([[]]);
          setAnimationPlaying(false);
          setAnimationFocus(0);
          setFocusedTile({ left: 0, top: 0 });
          setInputColor('#F5F5F5');
          setGhosting(true);
          setGhostingOpacity('0.5');
          localStorage.clear();
        }} >Reset</ResetStyled>
      </ControlStyled>
    </ContainerStyled>
  );
}
